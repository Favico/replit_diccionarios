"""
Dada una lista de países y ciudades de cada país, luego los nombres de las ciudades. Para cada ciudad, imprima el país en el que se encuentra.
Ejemplo:
2
Ecuador Quito Ambato Loja Cuenca
Peru Lima Cusco Piura Chiclayo
3
Loja
Ecuador
Piura
Peru
Cusco
Peru
"""
#Numero de listas a construir
n = int(input())
Diccionario ={}
for i in range (n):
#Aqui ingresamos el pais y depues la ciudades el splint hace que pueda ver un espacio entre cadenas.
#esto se va almacenar en un dicionario
    a = list(input().split())
#Las listas creadas van ir guardanose en un dicionario
    Diccionario[a[0]] = a[1:]
#Ingresamos el numero de ciudades las cuales queremos saber en que pais estan ubicadas y controlamos con un for
c = int(input())
for t in range (c):
#Ingresamo la ciudad
    b = input()
#Recorremos con for las listas en busca de la ciudad que queremos saber a que pais pertenece y nos imprime
    for pais, ciudad in Diccionario.items():
        if b in ciudad:
            print(pais)